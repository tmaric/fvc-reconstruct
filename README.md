# FOAM `fvc::reconstruct` operator tests 

This repository contains the test application that evaluates the convergence of the `fvc::reconstruct` operator in OpenFOAM [1] for an explicitly prescribed velocity and the two-phase flow velocity given by the [Hadamard–Rybczynski equation](https://en.wikipedia.org/wiki/Hadamard%E2%80%93Rybczynski_equation).

## Authors

* **Tomislav Maric** - *Development* - [MMA, TU Darmstadt](https://www.mma.tu-darmstadt.de/mma_institute/mma_team/maric_tomislav.en.jsp)

* **Tobias Tolle** - *Development* - [MMA, TU Darmstadt](https://www.mma.tu-darmstadt.de/mma_institute/mma_team/index.en.jsp)

* **Moritz Schwarzmeier** - *Development* - [MMA, TU Darmstadt](https://www.mma.tu-darmstadt.de/mma_institute/mma_team/contact_details_162432.en.jsp)

## Publications 

[1] [Tolle, T., Bothe, D., & Marić, T. (2020). SAAMPLE: A Segregated Accuracy-driven Algorithm for Multiphase Pressure-Linked Equations. Computers & Fluids, 200, 104450.](https://doi.org/10.1016/j.compfluid.2020.104450)

## Data

[Data produced by or related to this code can be found in this collection](https://tudatalib.ulb.tu-darmstadt.de/handle/tudatalib/1994)

## License

This project is licensed under the GPL v3 License.  

## Getting Started

### Prerequisites

Install OpenFOAM, any version should work. 

Tested with: 

* OpenFOAM-v2012

Compiler: g++ with "--std=c++2a" is required.

Tested with:

* g++ (GCC) 10.2.0 

For the parameter variation, `PyFoam` is needed, it's available via `pip`

```
?> sudo pip install PyFoam  
```

For data processing and visualization 

* Jupyter notebook or Jupyter-lab

### Installing

Compile the application

```
 fvc-reconstruct > ./Allwmake 
```

## Running the tests

Run the hex2D parameter study that varies the mesh density to compute the convergence 

```
?> cd data/hex2D 
?> bash ../pyFoamRunStudy hex2Dtemplate
```

This generates the output data, that is processed by the Jupyter notebook in the `cases` folder

```
cases > jupyter notebook fvc-reconstruct-convergence.ipynb
```
