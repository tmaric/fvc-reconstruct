#!/usr/bin/env python 

import pandas as pd
import sys

data_file_path=sys.argv[1]

data = pd.read_csv(data_file_path)
assert data["O(L_INF)"].min() > 1.7
