#!/usr/bin/python

import os
import pandas as pd
from math import log,nan
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pgf import FigureCanvasPgf

mpl.backend_bases.register_backend('pdf', FigureCanvasPgf)

def calc_convergences(values, deltas):
    """Compute a convergence rate for values as a function of deltas as
    log(value1 / value2) / log(delta1 / delta2)."""
    
    convergences = values.copy() 
    
    for i in range(len(values)-1):
        v1 = values.iloc[i] 
        v2 = values.iloc[i+1] 
        h1 = deltas.iloc[i] 
        h2 = deltas.iloc[i+1] 
        convergences[i] = log(v1 / v2) / log(h1 / h2)
        
    v1 = values.iloc[-2] 
    v2 = values.iloc[-1] 
    h1 = deltas.iloc[-2] 
    h2 = deltas.iloc[-1] 
    convergences[-1] = log(v1 / v2) / log(h1 / h2)
    
    return convergences 

def insert_convergences(valuesName, deltasName, dFrame):
    """Compute a convergence rate for values as a function of deltas as
    log(value1 / value2) / log(delta1 / delta2). and insert them into dFrame."""
    
    convs = calc_convergences(dFrame[valuesName], dFrame[deltasName])
    
    dFrame.insert(dFrame.columns.get_loc(valuesName)+1,  
                  "O(%s)" % valuesName, convs)
    
def analyze_test(testName):
    """Read test data and compute convergences of the measured and predicted error."""
    
    plt.style.use('seaborn-paper')
    mpl.rcParams['text.usetex'] = True
    mpl.rcParams['font.size'] = 32
    mpl.rcParams['lines.linewidth'] = 1.0
    mpl.rcParams['axes.grid'] = True
    mpl.rcParams['grid.linewidth'] = 0.5
    mpl.rcParams['grid.linestyle'] = 'dotted'
    mpl.rcParams['figure.dpi'] = 200
    mpl.rcParams['figure.figsize'] = 2.5,3
    
    test_data = pd.read_csv('%s.csv' % testName,comment="#", index_col=0)
    insert_convergences("L_INF", "H", test_data)
    insert_convergences("EPSILON_R_EXACT_MAX", "H", test_data)
    
    
    plt.rcParams.update({'font.size': 12})
    plt.loglog()
    plt.plot(test_data["H"], test_data["L_INF"], label="$L_{\infty}(\|\mathbf{v}_c - \mathbf{v}_c^R \|_2)$")
    plt.plot(test_data["H"], test_data["EPSILON_R_EXACT_MAX"], 'o', label="$L_{\infty}(\| \epsilon_c^{R, orth} \|_2)$")
    
    for i in range(len(test_data["H"])-1):
        plt.annotate("$%.3f$" % test_data["O(L_INF)"].iloc[i], 
                     (1.05  * test_data["H"].iloc[i], .9* test_data["L_INF"].iloc[i]),
                     fontsize=8)
    plt.legend()

    dataFileName = os.path.basename(testName) 

    # Save the table and figure to the data directory 
    dataDir = os.path.join(os.curdir, "fvc-reconstruct-data")
    if (not os.path.isdir(dataDir)):
        os.mkdir(dataDir)
    test_data.to_csv(os.path.join(dataDir, dataFileName + ".csv"))
    test_data.to_latex(os.path.join(dataDir, dataFileName + ".tex"))
