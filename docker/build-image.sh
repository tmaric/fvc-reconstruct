#!/usr/bin/env bash

# The image name must be the same as in .gitlab-ci.yml 
# for the GitLab CI to work, if the image is not uploaded
# to a publicly accessible container registry.
sudo docker build . -t openfoamv2012-ubuntu:focal \
    -f openfoamV2012-ubuntuFOCAL
