    volScalarField epsilonRmag
    (
        IOobject
        (
            "epsilonRmag", 
            mesh.time().timeName(), 
            mesh, 
            IOobject::NO_READ, 
            IOobject::AUTO_WRITE
        ),
        mesh, 
        dimensionedScalar("epsilonR", dimVelocity, scalar(0))
    );

    volVectorField epsilonRexactArea
    (
        IOobject
        (
            "epsilonRexactArea", 
            mesh.time().timeName(), 
            mesh, 
            IOobject::NO_READ, 
            IOobject::AUTO_WRITE
        ),
        mesh, 
        dimensionedVector("epslonRexactArea", dimVelocity * dimArea, vector(0,0,0))
    );

    volVectorField epsilonRexact
    (
        IOobject
        (
            "epsilonRexactArea", 
            mesh.time().timeName(), 
            mesh, 
            IOobject::NO_READ, 
            IOobject::AUTO_WRITE
        ),
        mesh, 
        dimensionedVector("epslonRexact", dimVelocity, vector(0,0,0))
    );


    volVectorField U
    (
        IOobject
        (
            "U", 
            mesh.time().timeName(), 
            mesh, 
            IOobject::NO_READ, 
            IOobject::AUTO_WRITE
        ),
        mesh, 
        dimensionedVector("U", dimVelocity, vector(0,0,0))
    );

    volTensorField gradU 
    (
        IOobject
        (
            "gradU", 
            mesh.time().timeName(), 
            mesh, 
            IOobject::NO_READ, 
            IOobject::AUTO_WRITE
        ),
        mesh, 
        dimensionedTensor("gradU", dimVelocity / dimLength, pTraits<Foam::tensor>::zero)
    );

    surfaceScalarField phi
    (
        IOobject
        (
            "phi", 
            mesh.time().timeName(), 
            mesh, 
            IOobject::NO_READ, 
            IOobject::AUTO_WRITE
        ),
        mesh, 
        dimensionedScalar("phi", dimVolume / dimTime, 0)
    );
