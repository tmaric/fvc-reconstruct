ARG UBUNTUVERSIONARG
FROM ubuntu:${UBUNTUVERSIONARG}

# avoid time zone configuration by tzdata
ARG DEBIAN_FRONTEND=noninteractive
ARG OPENFOAMVERSIONARG


RUN apt update
RUN apt upgrade -y
RUN apt install -y wget
RUN apt-get install -y libglu1-mesa
RUN wget -q -O - https://dl.openfoam.com/add-debian-repo.sh | bash
RUN apt-get update
RUN apt-get install -y "${OPENFOAMVERSIONARG}-default"

RUN "${OPENFOAMVERSIONARG}"
RUN echo $WM_PROJECT_DIR

#RUN ls /usr/lib/openfoam/openfoam2206/etc/
#RUN . /usr/lib/openfoam/openfoam2206/etc/bashrc
#RUN echo $WM_PROJECT_DIR


# Visualization and data processing

## Visualization packages
RUN apt-get install -y python3 \
    python3-matplotlib python3-numpy \
    python3-pandas python3-pip \
    jupyter-notebook jupyter-nbconvert python3-pip \
    texlive-full pandoc vim

## Default to python3 and pip3 
RUN update-alternatives --install /usr/bin/python python /usr/bin/python3 10 
RUN update-alternatives --install /usr/bin/pip pip /usr/bin/pip3 10 

## Install PyFoam for OpenFOAM parameter variation
RUN pip install PyFoam 
